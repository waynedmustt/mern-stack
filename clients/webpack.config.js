const path = require('path');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const CopyWebPackPlugin = require('copy-webpack-plugin');

var config = {
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: [
          'babel-loader'
        ]
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader'
          }
        ]
      },
      {
          test: /\.css$/,
          use: [
            'style-loader',
            'css-loader'
          ]
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          'file-loader'
        ]
      }
    ]
  },
  watch: true,
  watchOptions: {
    ignored: ['node_modules/**']
  },
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    writeToDisk: true,
    compress: true,
    port: 3300
  },
  plugins: [
      new HtmlWebPackPlugin({
        template: './src/index.html'
      }),
      new CopyWebPackPlugin([
        { from: 'src/assets', to: 'assets' }
      ])
  ],
  output: {
    filename: 'index.js',
    path: path.resolve(__dirname, 'dist'),
  },
  mode: 'development'
}

module.exports = (env, argv) => {
  if (argv.mode === 'production') {
    config.watch = false;
    config.watchOptions = {};
  }

  return config;
};