const Knex = require('knex');

let state = {
    db: null,
    promise: null
};

module.exports = {
    connect: function (options) {
        if (state.promise) {
            return state.promise;
        }

        const opts = options || {};
        const knex = new Knex({
            client: opts.client,
            connection: {
                host: opts.host,
                port: opts.port,
                user: opts.username,
                password: opts.password,
                database: opts.database
            }
        });
        const promise = new Promise(function(resolve) {
            state.db = knex;
            resolve(state.db);
        }).then(function () {
            return state.db;
        })
        .catch(function (err) {
            state.db = null;
            state.promise = null;
            throw err;
        });

        state.promise = promise;
        return state.promise;
    },
    get: function () {
        if (!state.db) {
            throw new Error('No database connection.');
        }

        return state.db;
    },
    close: function () {
        if (!state.db) {
            return;
        }

        state.db.destroy(function (err, result) {
            state.promise = null;
            state.db = null;
            if (done) {
                done(err, result);
            }
        });
    }
}