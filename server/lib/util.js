var path = require('path'),
    fs = require('fs'),
    merge = require('merge'),
    db = require('./db');

module.exports = {
    getConfig: function (configName) {
        const defaultPath = path.resolve(__dirname, '../etc/' + configName + '.json'),
            localPath = path.resolve(__dirname, '../etc/' + configName + '.local.json'),
            env = process.env.NODE_ENV || 'local';

        const envPath = path.resolve(__dirname, '../etc/' + configName + '.' + env + '.json');

        let defaultConfig = require(defaultPath);
        if (fs.existsSync(envPath)) {
            let envConfig = require(envPath);
            return merge.recursive(true, defaultConfig, envConfig);
        }

        if (fs.existsSync(localPath)) {
            let localConfig = require(localPath);
            return merge.recursive(true, defaultConfig, localConfig);
        }

        return defaultConfig;
    },
    hasDbInitialised: function (callback) {
        const knexInstance = db.get();
        knexInstance.schema.hasTable('ACL_permissions')
         .then(function (exists) {
             callback(exists);
         })
         .catch(function (err) {
             console.error(err);
             process.exit(1);
         });
    }
};