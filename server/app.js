const express = require('express')
const util = require('./lib/util')
const db = require('./lib/db')

let app = express()

const port = process.env.PORT || 3002
app.set('port', port);

const dbConfig = util.getConfig('database')
const dbPromise = db.connect(dbConfig.options);
const mysql = db.get()
app.set('dbPromise', dbPromise);

app.get('/', (req, res) => res.send('Hello World!'))
app.listen(port, () => console.log(`Example app listening on port ${port}!`))

app.get('dbPromise').then(function () {
    // ONLY FOR CONNECTION TESTING, SHOULD NOT BE HERE!
    util.hasDbInitialised(function(exists) {
        console.log('test', exists);
    });
    console.log('connected to database')
});