#!/bin/bash

if [ ! -d "/data/src/node_modules" ]; then
    cd /data/src
    npm install
fi

pm2 start app.js --name mern-server --log-date-format 'YYYY-MM-DD HH:mm Z'
sleep infinity
